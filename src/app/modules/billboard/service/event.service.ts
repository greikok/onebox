import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { Event } from '@shared/models/event.model';
import { SessionEvent } from '@shared/models/session.model';
import { environment } from '@env/environments';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  constructor(private http: HttpClient) { }

  getEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(environment.url.concat('events.json')).pipe(
      catchError(() => {
        return of([]);
      })
    );
  }

  getEvent(id: string): Observable<SessionEvent> {
    return this.http.get<SessionEvent>(environment.url.concat(`event-info-${id}.json`)).pipe(
      catchError(() => {
        return of();
      })
    );
  }
}

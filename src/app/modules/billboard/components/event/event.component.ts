import { CONST } from '@shared/const';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EventService } from '@modules/billboard/service/event.service';
import { Session, SessionEvent } from '@shared/models/session.model';
import { BreakpointService } from '@shared/services/breakpoint.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {
  CONST = CONST;
  id: string | null = null;
  cols$ = this.breakPointService.cols$;
  sessions: Session[];
  event: SessionEvent;
  constructor(private route: ActivatedRoute,
    private eventService: EventService,
    private cdRef: ChangeDetectorRef,
    private breakPointService: BreakpointService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.eventService.getEvent(this.id).subscribe(event => {
      event.sessions.forEach(session => {
        if (!session.selectedLocations) {
          session.selectedLocations = 0;
        }
      });

      this.sessions = JSON.parse(JSON.stringify(event.sessions));
      this.event = JSON.parse(JSON.stringify(event));
    });
  }

  onSessionUpdated(updatedSession: Session) {
    const currentSessions = this.event.sessions;
    const index = currentSessions.findIndex(session => session.date === updatedSession.date);
    if (index !== -1) {
      this.event = {
        ...this.event,
        sessions: currentSessions.map((session, i) => i === index ? updatedSession : session)
      };
    }
    this.cdRef.markForCheck();
  }

  onSessionRemoved(removedSession: Session) {
    const currentSessions = this.sessions;
    const index = currentSessions.findIndex(session => session.date === removedSession.date);
    if (index !== -1) {
      this.sessions = currentSessions.map((session, i) => i === index ? removedSession : session);
    }
    this.cdRef.markForCheck();
  }
}

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { EventService } from './service/event.service';
import { Observable } from 'rxjs';
import { Event } from '@shared/models/event.model';
import { BreakpointService } from '@shared/services/breakpoint.service';

@Component({
  selector: 'app-billboard',
  templateUrl: './billboard.component.html',
  styleUrls: ['./billboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BillboardComponent implements OnInit {
  events$: Observable<Event[]>;
  cols$ = this.breakPointService.cols$;

  constructor(
    private evetService: EventService,
    private breakPointService: BreakpointService
  ) { }

  ngOnInit(): void {
    this.events$ = this.evetService.getEvents();
  }
}

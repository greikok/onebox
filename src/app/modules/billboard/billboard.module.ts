import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillboardRoutingModule } from './billboard-routing.module';
import { SharedModule } from '@shared/shared.module';
import { BillboardComponent } from './billboard.component';
import { LayoutModule } from '@angular/cdk/layout';
import { EventComponent } from './components/event/event.component';

@NgModule({
  declarations: [
    BillboardComponent,
    EventComponent
  ],
  imports: [
    CommonModule,
    BillboardRoutingModule,
    SharedModule,
    LayoutModule
  ]
})
export class BillboardModule { }

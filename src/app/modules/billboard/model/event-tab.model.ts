export interface EventTab {
  id: string,
  title: string,
  subtitle: string,
  image: string,
}

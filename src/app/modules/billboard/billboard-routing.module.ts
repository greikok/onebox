// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BillboardComponent } from './billboard.component';
import { EventComponent } from './components/event/event.component';

const routes: Routes = [
  {
    path: '',
    component: BillboardComponent
  },
  {
    path: 'event/:id',
    component: EventComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillboardRoutingModule { }

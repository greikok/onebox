import { Component } from '@angular/core';
import { CONST } from '@shared/const';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent {
  CONST = CONST;
}

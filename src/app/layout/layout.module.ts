// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
// Components
import { LayoutComponent } from '@layout/layout.component';
// Material
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
// Modules
import { LayoutRoutingModule } from '@layout/layout-routing.module';
import { SharedModule } from '@shared/shared.module';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [
    LayoutComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    LayoutRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule
  ]
})
export class LayoutModule { }

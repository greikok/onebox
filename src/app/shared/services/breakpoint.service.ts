import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BreakpointService {
  private _cols = new BehaviorSubject<number>(1);
  public readonly cols$ = this._cols.asObservable();

  constructor(private breakpointObserver: BreakpointObserver) {
    this.breakpointObserver.observe([
      Breakpoints.Small,
      Breakpoints.Medium,
      Breakpoints.Large,
      Breakpoints.XLarge
    ])
      .pipe(
        map(breakpointState => {
          if (breakpointState.breakpoints[Breakpoints.Medium] ||
            breakpointState.breakpoints[Breakpoints.Large] ||
            breakpointState.breakpoints[Breakpoints.XLarge]) {
            return 2;
          }
          return 1;
        })
      ).subscribe(result => {
        this._cols.next(result);
      });
  }

  getCurrentColsValue(): number {
    return this._cols.getValue();
  }
}

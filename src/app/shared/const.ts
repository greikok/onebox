export class CONST {
  // HEADER
  public static readonly HEADER_TITLE = 'header.title';
  // CARD
  public static readonly CARD_BUTTON = 'card.button';
  // EVENT
  public static readonly EVENT_BUTTON_BACK = 'event.buttonBack';
  // SESSION
  public static readonly SESSION_AVAILABILITY = 'session.availability';
  public static readonly SESSION_NOTFOUND = 'session.notfound';
  // GENERAL APP
  public static readonly APP_DATE = 'app.date';
}

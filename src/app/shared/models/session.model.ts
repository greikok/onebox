export interface Session {
  date: string,
  availability: string,
  selectedLocations?: number;
}
export interface Event {
  id: string,
  title: string,
  subtitle: string,
  image: string
}

export interface SessionEvent {
  event: Event,
  sessions: Session[]
}

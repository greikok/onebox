import { CONST } from '@shared/const';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Session } from '@shared/models/session.model';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SessionComponent {
  @Input() eventSession!: Session[];
  @Output() sessionUpdated = new EventEmitter<Session>();
  CONST = CONST;

  addLocation(session: Session) {
    if (session.selectedLocations < Number(session.availability)) {
      session.selectedLocations++;
      this.sessionUpdated.emit(session);
    }
  }

  dropLocation(session: Session) {
    if (session.selectedLocations > 0) {
      session.selectedLocations--;
      this.sessionUpdated.emit(session);
    }
  }
}

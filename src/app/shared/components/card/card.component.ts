import { CONST } from '@shared/const';
import { Component, Input } from '@angular/core';
import { Event } from '@shared/models/event.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
@Input() card: Event | undefined;
@Input() column: number | null | undefined;
CONST = CONST;
}

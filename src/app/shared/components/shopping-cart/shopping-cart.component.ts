import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { CONST } from '@shared/const';
import { Session, SessionEvent } from '@shared/models/session.model';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShoppingCartComponent {
  @Input() eventSession!: SessionEvent;
  @Output() removeSession = new EventEmitter<Session>();
  CONST = CONST;

  sessions: SessionEvent[] = [];

  constructor() { }

  removeFromCart(session: Session) {
    if (this.hasLocationSelected(session.selectedLocations)) {
      session.selectedLocations -= 1;
      this.removeSession.emit(session);
    }
  }

  hasLocationSelected(selectedLocations: number):Boolean {
    return selectedLocations > 0;
  }
}

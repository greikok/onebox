import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortByDate'
})
export class SortByDatePipe implements PipeTransform {
  transform(events: any[], property: string = 'endDate'): any[] {
    if (!Array.isArray(events) || !property) {
      return events;
    }
    return events.sort((a, b) => +a[property] - +b[property]);
  }
}

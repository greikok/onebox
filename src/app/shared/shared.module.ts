// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Material
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
// NGX Translate
import { TranslateModule } from '@ngx-translate/core';
// Components
import { CardComponent } from './components/card/card.component';
import { DatePipe } from './pipes/date.pipe';
import { RouterModule } from '@angular/router';
import { SessionComponent } from './components/session/session.component';
import { SortByDatePipe } from './pipes/sort-by-date.pipe';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';

@NgModule({
  declarations: [
    CardComponent,
    DatePipe,
    SessionComponent,
    SortByDatePipe,
    ShoppingCartComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    RouterModule,
    MatIconModule
  ],
  exports: [
    TranslateModule,
    CardComponent,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    RouterModule,
    SessionComponent,
    SortByDatePipe,
    ShoppingCartComponent,
    MatIconModule
  ]
})
export class SharedModule { }
